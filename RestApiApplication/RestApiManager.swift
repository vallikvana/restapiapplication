//
//  RestApiManager.swift
//  RestApiApplication
//
//  Created by kvanaMini1 on 14/07/16.
//  Copyright © 2016 kvanaMini1. All rights reserved.
//

import Foundation

typealias serivceResponse = (JSON,NSError?) -> Void

class RestApiManager: NSObject {
    
    static let sharedInstance = RestApiManager()
    let baseURL = "http://api.randomuser.me/"
    
    func getRandomUser(onCompletion: (JSON) ->Void) {
        makeHttpGetRequest(baseURL, onCompletion: {json, err ->Void in
            onCompletion(json)
        })
    }
    
    func makeHttpGetRequest(path: String, onCompletion: serivceResponse){
        let request = NSMutableURLRequest(URL: NSURL(string: path)!)
        request.HTTPMethod = "GET"
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request, completionHandler: { data, response, error in
            let json: JSON = JSON(data!)
            onCompletion(json, error)
        })
        task.resume()
        
    }
    
}
