//
//  ViewController.swift
//  RestApiApplication
//
//  Created by kvanaMini1 on 14/07/16.
//  Copyright © 2016 kvanaMini1. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    var tableView:UITableView?
    var items = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        let frame:CGRect = CGRect(x: 0, y: 100, width: self.view.frame.width, height: self.view.frame.height-100)
        self.tableView = UITableView(frame: frame)
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.view.addSubview(self.tableView!)
        
        let button = UIButton(frame:CGRect(x: 0, y: 25, width: self.view.frame.width, height: 50))
        button.backgroundColor = UIColor.lightGrayColor()
        button.setTitle("Details", forState: UIControlState.Normal)
        button.addTarget(self, action: "addDummydata", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
        
    }
    
    func addDummydata(){
        RestApiManager.sharedInstance.getRandomUser{ json -> Void in
            let results = json["results"]
            for (index, subJson):(String, JSON)  in results{
                let user:AnyObject = subJson["user"].object
                self.items.addObject(user)
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView?.reloadData()
                })
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell!=UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "tableviewcell")
        let user: JSON = JSON(self.items[indexPath.row])
        let picsURL = user["picture"]["medium"].string
        let url = NSURL(string: picsURL!)
        let data = NSData(contentsOfURL: url!)

        cell.textLabel?.text = items[indexPath.row] as! String
        cell.imageView!.image = UIImage(data: data!)
        
        return UITableViewCell()
    }
}

